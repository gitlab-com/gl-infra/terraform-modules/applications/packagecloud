# Packagecloud Terraform Module

## What is this?

Packagecloud runs on Kubernetes (managed by Helm chart) and requires many resources managed by this module.
The majority of the resources live in GCP with only a few in AWS.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.5 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 4.0 |
| <a name="requirement_cloudflare"></a> [cloudflare](#requirement\_cloudflare) | >= 4.39.0, < 5 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 5.6.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 4.0 |
| <a name="provider_aws.replication"></a> [aws.replication](#provider\_aws.replication) | >= 4.0 |
| <a name="provider_cloudflare"></a> [cloudflare](#provider\_cloudflare) | >= 4.39.0, < 5 |
| <a name="provider_google"></a> [google](#provider\_google) | >= 5.6.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_mysql"></a> [mysql](#module\_mysql) | GoogleCloudPlatform/sql-db/google//modules/mysql | 25.1.0 |
| <a name="module_redis"></a> [redis](#module\_redis) | terraform-google-modules/memorystore/google | 13.1.0 |
| <a name="module_sql-proxy-workload-identity"></a> [sql-proxy-workload-identity](#module\_sql-proxy-workload-identity) | terraform-google-modules/kubernetes-engine/google//modules/workload-identity | 35.0.1 |

## Resources

| Name | Type |
|------|------|
| [aws_iam_access_key.packagecloud](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_access_key) | resource |
| [aws_iam_policy.cloudfront](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.iam](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.replication](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.s3](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.replication](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.replication](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_user.packagecloud](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user) | resource |
| [aws_iam_user_policy_attachment.cloudfront](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user_policy_attachment) | resource |
| [aws_iam_user_policy_attachment.iam](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user_policy_attachment) | resource |
| [aws_iam_user_policy_attachment.s3](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user_policy_attachment) | resource |
| [aws_s3_bucket.packages](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.replication](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_lifecycle_configuration.packages](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_lifecycle_configuration) | resource |
| [aws_s3_bucket_replication_configuration.replication](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_replication_configuration) | resource |
| [aws_s3_bucket_versioning.packages](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_versioning) | resource |
| [aws_s3_bucket_versioning.replication](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_versioning) | resource |
| [cloudflare_record.packages-gitlab-com](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/record) | resource |
| [google_compute_global_address.ingress](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_global_address) | resource |
| [google_project_iam_member.wi-cloudsql](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.assume_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.cloudfront](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.iam](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.replication](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.s3](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_tags"></a> [aws\_tags](#input\_aws\_tags) | A mapping of tags to assign to all AWS resources. | `map(string)` | `{}` | no |
| <a name="input_bucket_name"></a> [bucket\_name](#input\_bucket\_name) | S3 bucket name that contains the DEB/RPM/etc packages. | `string` | n/a | yes |
| <a name="input_bucket_replication"></a> [bucket\_replication](#input\_bucket\_replication) | S3 replication bucket settings. | <pre>object({<br/>    name          = optional(string, "")<br/>    storage_class = optional(string, "STANDARD_IA")<br/>  })</pre> | `{}` | no |
| <a name="input_bucket_versioning_enabled"></a> [bucket\_versioning\_enabled](#input\_bucket\_versioning\_enabled) | Boolean to control if object versioning should be enabled. | `bool` | `true` | no |
| <a name="input_cloudflare_dns_record_name"></a> [cloudflare\_dns\_record\_name](#input\_cloudflare\_dns\_record\_name) | Optional (when creating Cloudflare DNS record): DNS record name -- eg. packages | `string` | `""` | no |
| <a name="input_cloudflare_dns_record_value"></a> [cloudflare\_dns\_record\_value](#input\_cloudflare\_dns\_record\_value) | Optional (when creating Cloudflare DNS record): IP address the DNS record should point to. | `string` | `""` | no |
| <a name="input_cloudflare_zone_id"></a> [cloudflare\_zone\_id](#input\_cloudflare\_zone\_id) | Optional (when creating Cloudflare DNS record): Cloudflare zone ID. | `string` | `""` | no |
| <a name="input_cloudsql_availability_type"></a> [cloudsql\_availability\_type](#input\_cloudsql\_availability\_type) | The availability type for the master instance. | `string` | `"REGIONAL"` | no |
| <a name="input_cloudsql_backup_configuration"></a> [cloudsql\_backup\_configuration](#input\_cloudsql\_backup\_configuration) | The backup\_configuration settings subblock for the database setings | <pre>object({<br/>    binary_log_enabled             = optional(bool, false)<br/>    enabled                        = optional(bool, false)<br/>    start_time                     = optional(string)<br/>    location                       = optional(string)<br/>    transaction_log_retention_days = optional(string)<br/>    retained_backups               = optional(number)<br/>    retention_unit                 = optional(string)<br/>  })</pre> | `{}` | no |
| <a name="input_cloudsql_database_flags"></a> [cloudsql\_database\_flags](#input\_cloudsql\_database\_flags) | List of Cloud SQL flags that are applied to the database server. See [more details](https://cloud.google.com/sql/docs/mysql/flags) | <pre>list(object({<br/>    name  = string<br/>    value = string<br/>  }))</pre> | <pre>[<br/>  {<br/>    "name": "sql_mode",<br/>    "value": "STRICT_ALL_TABLES,NO_AUTO_VALUE_ON_ZERO"<br/>  }<br/>]</pre> | no |
| <a name="input_cloudsql_database_version"></a> [cloudsql\_database\_version](#input\_cloudsql\_database\_version) | The database version to use. | `string` | n/a | yes |
| <a name="input_cloudsql_deletion_protection_enabled"></a> [cloudsql\_deletion\_protection\_enabled](#input\_cloudsql\_deletion\_protection\_enabled) | Enables protection of an instance from accidental deletion across all surfaces (API, gcloud, Cloud Console and Terraform). | `bool` | `true` | no |
| <a name="input_cloudsql_disk_size"></a> [cloudsql\_disk\_size](#input\_cloudsql\_disk\_size) | Disk size for the DB. | `number` | `50` | no |
| <a name="input_cloudsql_insights_config"></a> [cloudsql\_insights\_config](#input\_cloudsql\_insights\_config) | The insights\_config settings for the database. | <pre>object({<br/>    query_plans_per_minute  = number<br/>    query_string_length     = number<br/>    record_application_tags = bool<br/>    record_client_address   = bool<br/>  })</pre> | `null` | no |
| <a name="input_cloudsql_ip_configuration"></a> [cloudsql\_ip\_configuration](#input\_cloudsql\_ip\_configuration) | The ip\_configuration settings subblock | <pre>object({<br/>    authorized_networks                           = list(map(string))<br/>    ipv4_enabled                                  = bool<br/>    private_network                               = string<br/>    ssl_mode                                      = string<br/>    allocated_ip_range                            = string<br/>    enable_private_path_for_google_cloud_services = optional(bool)<br/>  })</pre> | n/a | yes |
| <a name="input_cloudsql_tier"></a> [cloudsql\_tier](#input\_cloudsql\_tier) | The tier for the master instance. | `string` | n/a | yes |
| <a name="input_cloudsql_zone_master_instance"></a> [cloudsql\_zone\_master\_instance](#input\_cloudsql\_zone\_master\_instance) | The zone for the master instance, it should be something like: `us-central1-a`, `us-east1-c`. | `string` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment for the packagecloud:enterprise installation. | `string` | n/a | yes |
| <a name="input_gcp_project_id"></a> [gcp\_project\_id](#input\_gcp\_project\_id) | The GCP project ID to manage the GCP resources. | `string` | n/a | yes |
| <a name="input_gcp_region"></a> [gcp\_region](#input\_gcp\_region) | The region of the GCP resources. | `string` | n/a | yes |
| <a name="input_memorystore_authorized_network"></a> [memorystore\_authorized\_network](#input\_memorystore\_authorized\_network) | The full name of the Google Compute Engine network to which the instance is connected. | `string` | `null` | no |
| <a name="input_memorystore_memory_size_gb"></a> [memorystore\_memory\_size\_gb](#input\_memorystore\_memory\_size\_gb) | Redis memory size in GiB | `number` | n/a | yes |
| <a name="input_memorystore_redis_configs"></a> [memorystore\_redis\_configs](#input\_memorystore\_redis\_configs) | The Redis configuration parameters. See [more details](https://cloud.google.com/memorystore/docs/redis/reference/rest/v1/projects.locations.instances#Instance.FIELDS.redis_configs) | `map(any)` | `{}` | no |
| <a name="input_memorystore_redis_version"></a> [memorystore\_redis\_version](#input\_memorystore\_redis\_version) | The version of Redis software. | `string` | n/a | yes |
| <a name="input_memorystore_tier"></a> [memorystore\_tier](#input\_memorystore\_tier) | The service tier of the instance. https://cloud.google.com/memorystore/docs/redis/reference/rest/v1/projects.locations.instances#Tier | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
