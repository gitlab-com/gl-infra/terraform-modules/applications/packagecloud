terraform {
  required_version = ">= 1.5"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.0"

      configuration_aliases = [aws.replication]
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = ">= 4.39.0, < 5"
    }
    google = {
      source  = "hashicorp/google"
      version = ">= 5.6.0"
    }
  }
}
