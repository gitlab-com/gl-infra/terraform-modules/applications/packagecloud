terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

module "packagecloud" {
  source = "../.."

  providers = {
    aws.replication = aws
  }

  environment = "env"

  gcp_region     = "region"
  gcp_project_id = "project"

  bucket_name = "bucket-name"

  cloudsql_database_version     = "MYSQL_8_0"
  cloudsql_tier                 = "tier"
  cloudsql_zone_master_instance = "zone"
  cloudsql_ip_configuration = {
    ipv4_enabled        = false
    private_network     = "private-network"
    ssl_mode            = "ENCRYPTED_ONLY"
    allocated_ip_range  = null
    authorized_networks = []
  }

  memorystore_tier           = "BASIC"
  memorystore_memory_size_gb = 1
  memorystore_redis_version  = "REDIS_6_X"
  memorystore_redis_configs  = {}
}
